// /src/pages/index.tsx

import * as React from "react";
import loadable from "@loadable/component";
import type { HeadFC } from "gatsby";

const IndexPage = () => {
  const Plot = loadable(() => import("react-plotly.js"));
  function createTraces(): Partial<Plotly.PlotData>[] {
    return [
      {
        name: "trace1",
        text: "Trace One",
        type: "scattermapbox",
        mode: "markers",
        lat: [CENTER.lat],
        lon: [CENTER.lon],
        marker: {
          size: 30,
          color: "red",
          opacity: 0.5,
        },
        hoverinfo: "text",
      },
      {
        name: "trace2",
        text: "Trace Two",
        type: "scattermapbox",
        mode: "markers",
        lat: [42.8248],
        lon: [-83.2647],
        marker: {
          size: 20,
          color: "blue",
          opacity: 0.5,
        },
        hoverinfo: "text",
      },
    ];
  }
  function createFrames(): Partial<Plotly.Frame>[] {
    return [
      {
        name: `frame_0`,
        data: [
          {
            marker: {
              size: 30,
              color: "red",
              opacity: 0.5,
            },
          },
          {
            lat: [42.8248],
            lon: [-83.2547],
          },
        ],
      },
      {
        name: `frame_1`,
        data: [
          {
            marker: {
              size: 45,
              color: "orange",
              opacity: 0.75,
            },
          },
          {
            lat: [42.8248],
            lon: [-83.2447],
          },
        ],
      },
      {
        name: `frame_2`,
        data: [
          {
            marker: {
              size: 50,
              color: "yellow",
              opacity: 1.0,
            },
          },
          {
            lat: [42.8248],
            lon: [-83.2347],
          },
        ],
      },
    ];
  }
  const CENTER = { lat: 42.838249, lon: -83.200787 };
  function createLayout(): Partial<Plotly.Layout> {
    return {
      datarevision: 1,
      autosize: false,
      height: 500,
      title: "Example Animated Map",
      hovermode: "closest",
      showlegend: false,
      xaxis: { visible: false },
      yaxis: { visible: false },
      mapbox: {
        style: "carto-positron",
        zoom: 10,
        center: CENTER,
      },
      updatemenus: [
        {
          x: 0,
          y: 0,
          yanchor: "top",
          xanchor: "left",
          showactive: false,
          direction: "left",
          type: "buttons",
          pad: { t: 87, r: 10 },
          buttons: [
            {
              method: "animate",
              args: [
                null,
                {
                  mode: "immediate",
                  fromcurrent: true,
                  transition: { duration: 300 },
                  frame: { duration: 500, redraw: true },
                },
              ],
              label: "Play",
            },
            {
              method: "animate",
              args: [
                null,
                {
                  mode: "immediate",
                  transition: { duration: 0 },
                  frame: { duration: 0, redraw: false },
                },
              ],
              label: "Pause",
            },
          ],
        },
      ],
      // Finally, add the slider and use `pad` to position it
      // nicely next to the buttons.
      sliders: [
        {
          pad: { l: 130, t: 55 },
          currentvalue: {
            visible: true,
            prefix: "Frame:",
            xanchor: "right",
            font: { size: 20, color: "#666" },
          },
          steps: [
            {
              method: "animate",
              label: "0",
              value: "frame_0",
              args: [
                ["frame_0"],
                {
                  mode: "immediate",
                  transition: { duration: 300 },
                  frame: { duration: 300, redraw: true },
                },
              ],
            },
            {
              method: "animate",
              label: "1",
              value: "frame_1",
              args: [
                ["frame_1"],
                {
                  mode: "immediate",
                  transition: { duration: 300 },
                  frame: { duration: 300, redraw: true },
                },
              ],
            },
            {
              method: "animate",
              label: "2",
              value: "frame_2",
              args: [
                ["frame_2"],
                {
                  mode: "immediate",
                  transition: { duration: 300 },
                  frame: { duration: 300, redraw: true },
                },
              ],
            },
          ],
        },
      ],
    };
  }
  function createConfiguration(): Partial<Plotly.Config> {
    return {};
  }
  return (
    <main>
      <h1>Animated Map Plot Example</h1>
      <Plot
        data={createTraces()}
        frames={createFrames()}
        layout={createLayout()}
        config={createConfiguration()}
      />
    </main>
  );
};

export default IndexPage;
